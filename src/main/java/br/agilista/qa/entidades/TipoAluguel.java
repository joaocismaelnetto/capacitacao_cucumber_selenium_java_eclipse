package br.agilista.qa.entidades;

public enum TipoAluguel 
{
	COMUM, EXTENDIDO, SEMANAL;
}
