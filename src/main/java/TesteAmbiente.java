import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteAmbiente 
{
	//*
	//* Declarando objetos do tipo WebDriver
		
	private static WebDriver  driver;
	
	public static void main(String[] args) 
	{
		//*
		//* Open Chrome Drive Conection			

		System.out.println("Opening ChromeDriver connection...");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\joanetto\\Drivers\\chromedriver-win64\\chromedriver.exe");
		driver = new ChromeDriver();

	    
		//*
		//* Open Webview Advantage On Line Shopping

		String baseUrl = "https://seubarriga.wcaquino.me/";
		driver.get(baseUrl);
        System.out.println(driver.getTitle());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);		

	}
	

}
