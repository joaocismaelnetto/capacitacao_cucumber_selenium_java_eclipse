package br.agilista.qa.steps;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import junit.framework.Assert;

public class InserirContasSteps 
{
	//*
	//* Declarando objetos do tipo WebDriver
		
	private static WebDriver  driver;
	
	//**> Imperativas
	
	@Dado("^que desejo adicionais uma conta$")
	public void que_desejo_adicionais_uma_conta() throws Throwable 
	{
		//*
		//* Open Chrome Drive Conection	IMPERATIVO		

		System.out.println("Opening ChromeDriver connection... FOR RESET DATA BASE");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\joanetto\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	    
		//*
		//* Open Webview sr barriga Open Chrome Drive Conection	IMPERATIVO	

		String baseUrl = "https://seubarriga.wcaquino.me/";
		driver.get(baseUrl);
        System.out.println(driver.getTitle());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//*
		//* USER  Open Chrome Drive Conection	IMPERATIVO		
			
		WebElement sendEmail = driver.findElement(By.id("email"));
		sendEmail.sendKeys("joaoismael@os-caminhantes.com");		
		
		//*
		//* PASSWORD  Open Chrome Drive Conection	IMPERATIVO	
		
		WebElement sendSenha = driver.findElement(By.id("senha"));
		sendSenha.sendKeys("Tanisma01!");
		
		//*
		//* ENTRAR  Open Chrome Drive Conection	IMPERATIVO	
		
		WebElement clickButtonEntrar = driver.findElement(By.tagName("button"));
		clickButtonEntrar .click();
		
		//*
		//* Clico em contas IMPERATIVO	
		
		WebElement clickContas = driver.findElement(By.linkText("Contas"));
		clickContas.click();		
		System.out.println("sr.barriga - login click Entrar: " + clickContas);
		
		//*
		//* Clico em adicionar IMPERATIVO	
		
		WebElement clickContasAdicionar = driver.findElement(By.linkText("Adicionar"));
		clickContasAdicionar.click();		
		System.out.println("sr.barriga - login click Entrar: " + clickContasAdicionar);	
	}	
	
	
	@Quando("^adiciono a conta \"([^\"]*)\"$")
	public void adiciono_a_conta(String arg1) throws Throwable 
	{
		System.out.println("sr.barriga - conta para adição: " + arg1);		
		WebElement sendConta = driver.findElement(By.id("nome"));
		sendConta.sendKeys(arg1);		

		WebElement clickButtonSalvar = driver.findElement(By.tagName("button"));
		clickButtonSalvar .click();		
		System.out.println("sr.barriga - login click Salvar: " + clickButtonSalvar);	
	}
	
	
	@Então("^recebo a mensagem \"([^\"]*)\"$")
	public void receboAMensagem(String arg1) throws Throwable 
	{
	
		String titleContaExist = driver.findElement(By.xpath("//div[starts-with(@class, 'alert alert-')]")).getText();
		System.out.println("sr.barriga - titulo: " + titleContaExist);
		
		assertEquals(arg1, titleContaExist);
	}
	
			
	@After (order = 0, value = {"~@functionals"})
	public void fecharBrowser() 
	{
		System.out.println("closing ChromeDriver connection...... FOR FUNCTION TEST");
		driver.quit();
	}
	
	
	@After(order = 1, value = {"~@functionals"})
	public void screenshot(Scenario cenario) 
	{
		File screenshotAs = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshotAs, new File("target/screenshots/"+cenario.getId()+".jpg"));
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
}
