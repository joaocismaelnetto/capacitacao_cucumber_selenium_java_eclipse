package br.agilista.qa.runners;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions
(
//	features   = "src/test/resources/features/cenario_gestao_contas_consumo_sr_barriga.feature",
		features   = "src/test/resources/features/",
		glue       = "br.agilista.qa.steps",
//		tags       = {"~@ignore"},
		tags       = {"@functionals"},
		plugin     = {"pretty", "html:target/report-html", "json:target/report-json"},
		monochrome = true,
		snippets   = SnippetType.CAMELCASE,
		dryRun     = false,
		strict     = false
 )
public class RunnerFunctionalTest 
{
	@BeforeClass 
		public static void reset() 
	{
		
		//*
		//* Open Chrome Drive Conection	RESET DATA BASE		

		System.out.println("Opening ChromeDriver connection... FOR RESET DATA BASE");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\joanetto\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	    
		//*
		//* Open Webview sr barriga RESET DATA BASE

		String baseUrl = "https://seubarriga.wcaquino.me/";
		driver.get(baseUrl);
        System.out.println(driver.getTitle());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//*
		//* USER RESET DATA BASE		
			
		WebElement sendEmail = driver.findElement(By.id("email"));
		sendEmail.sendKeys("joaoismael@os-caminhantes.com");		
		
		//*
		//* PASSWORD RESET DATA BASE RESET DATA BASE
		
		WebElement sendSenha = driver.findElement(By.id("senha"));
		sendSenha.sendKeys("Tanisma01!");
		
		//*
		//* ENTRAR RESET DATA BASE 
		
		WebElement clickButtonEntrar = driver.findElement(By.tagName("button"));
		clickButtonEntrar .click();		
		
		//*
		//* CLICK PASSWORD RESET DATA BASE
		
		WebElement clickReset = driver.findElement(By.linkText("reset"));
		clickReset.click();
		
		//*
		//* CLOSE Chrome Drive Conection
		
		System.out.println("closing ChromeDriver connection...... FOR RESET DATA BASE");
		driver.quit();		
	}
		
}

