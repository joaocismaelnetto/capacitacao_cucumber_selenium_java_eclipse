package br.agilista.qa.runners;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions
(
	//	features   = "src/test/resources/features/cenario_gestao_contas_consumo_sr_barriga.feature",
		features   = "src/test/resources/features/",
		glue       = "br.agilista.qa.steps",
//		tags       = {"~@ignore"},
		tags       = {"@unitarios"},
		plugin     = {"pretty", "html:target/report-html", "json:target/report-json"},
		monochrome = true,
		snippets   = SnippetType.CAMELCASE,
		dryRun     = false,
		strict     = false
 )
public class RunnerTest 
{
			
}

