package br.agilista.qa.steps;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import junit.framework.Assert;

public class InserirContasDeclarativasSteps 
{
	//*
	//* Declarando objetos do tipo WebDriver
		
	private static WebDriver  driver;
	
	//**> Imperativas
	
	@Dado("^que desejo adicionais uma conta$")
	public void que_desejo_adicionais_uma_conta() throws Throwable 
	{
		//*
		//* Open Chrome Drive Conection	IMPERATIVO		

		System.out.println("Opening ChromeDriver connection... FOR RESET DATA BASE");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\joanetto\\Drivers\\chromedriver-win64\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	    
		//*
		//* Open Webview sr barriga Open Chrome Drive Conection	IMPERATIVO	

		String baseUrl = "https://seubarriga.wcaquino.me/";
		driver.get(baseUrl);
        System.out.println(driver.getTitle());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//*
		//* USER  Open Chrome Drive Conection	IMPERATIVO		
			
		WebElement sendEmail = driver.findElement(By.id("email"));
		sendEmail.sendKeys("joaoismael@os-caminhantes.com");		
		
		//*
		//* PASSWORD  Open Chrome Drive Conection	IMPERATIVO	
		
		WebElement sendSenha = driver.findElement(By.id("senha"));
		sendSenha.sendKeys("Tanisma01!");
		
		//*
		//* ENTRAR  Open Chrome Drive Conection	IMPERATIVO	
		
		WebElement clickButtonEntrar = driver.findElement(By.tagName("button"));
		clickButtonEntrar .click();		
	}

	@Quando("^adiciono a conta \"([^\"]*)\"$")
	public void adiciono_a_conta(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
	
	//**> Declarativas 	
	
	
	@Dado("^que estou acessando a aplicação$")
	public void queEstouAcessandoAAplicação() throws Throwable 
	{
		//*
		//* Open Chrome Drive Conection			

		//System.out.println("Opening ChromeDriver connection...");
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\joanetto\\Drivers\\chromedriver-win64\\chromedriver.exe");
		driver = new ChromeDriver();
	    
		//*
		//* Open Webview sr barriga

		String baseUrl = "https://seubarriga.wcaquino.me/";
		driver.get(baseUrl);
        System.out.println(driver.getTitle());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Quando("^informo o usuário \"([^\"]*)\"$")
	public void informoOUsuário(String arg1) throws Throwable 
	{
		System.out.println("sr.barriga - login email: " + arg1);		
		WebElement sendEmail = driver.findElement(By.id("email"));
		sendEmail.sendKeys(arg1);		
	}

	@E("^a senha \"([^\"]*)\"$")
	public void aSenha(String arg1) throws Throwable 
	{
		System.out.println("sr.barriga - login senha: " + arg1);		
		WebElement sendSenha = driver.findElement(By.id("senha"));
		sendSenha.sendKeys(arg1);		
	}	

	@E("^seleciono entrar$")
	public void selecionoEntrar() throws Throwable 
	{
		WebElement clickButtonEntrar = driver.findElement(By.tagName("button"));
		clickButtonEntrar .click();		
		System.out.println("sr.barriga - login click Entrar: " + clickButtonEntrar);		
	}

	@Então("^visualizo a página inicial$")
	public void visualizoAPáginaInicial() throws Throwable 
	{
		String titleBemVindo = driver.findElement(By.xpath("//div[@class='alert alert-success']")).getText();
		System.out.println("sr.barriga - titulo: " + titleBemVindo);
		
		assertEquals("Bem vindo, joaoismael!", titleBemVindo);
	}

	@Quando("^seleciono Contas$")
	public void selecionoContas() throws Throwable 
	{
		WebElement clickContas = driver.findElement(By.linkText("Contas"));
		clickContas.click();		
		System.out.println("sr.barriga - login click Entrar: " + clickContas);	
	}

	@E("^seleciono Adicionar$")
	public void selecionoAdicionar() throws Throwable 
	{
		WebElement clickContasAdicionar = driver.findElement(By.linkText("Adicionar"));
		clickContasAdicionar.click();		
		System.out.println("sr.barriga - login click Entrar: " + clickContasAdicionar);	
	}

	@Quando("^informo a conta \"([^\"]*)\"$")
	public void informoAConta(String arg1) throws Throwable 
	{
		System.out.println("sr.barriga - conta para adição: " + arg1);		
		WebElement sendConta = driver.findElement(By.id("nome"));
		sendConta.sendKeys(arg1);		
	}

	@E("^seleciono Salvar$")
	public void selecionoSalvar() throws Throwable 
	{
		WebElement clickButtonSalvar = driver.findElement(By.tagName("button"));
		clickButtonSalvar .click();		
		System.out.println("sr.barriga - login click Salvar: " + clickButtonSalvar);		
	}

	@Então("^a conta é inserida com sucesso$")
	public void aContaÉInseridaComSucesso() throws Throwable 
	{
		String titleContaInserida = driver.findElement(By.xpath("//div[@class='alert alert-success']")).getText();
		System.out.println("sr.barriga - titulo: " + titleContaInserida);
		
		assertEquals("Conta adicionada com sucesso!", titleContaInserida);
	}
	
	@Então("^sou notificado que o nome da conta é obrigatório$")
	public void souNotificarQueONomeDaContaÉObrigatório() throws Throwable 
	{
		String titleContaObrig = driver.findElement(By.xpath("//div[@class='alert alert-danger']")).getText();
		System.out.println("sr.barriga - titulo: " + titleContaObrig);
		
		assertEquals("Informe o nome da conta", titleContaObrig);
	}
	
	@Então("^sou notificado que já existe uma conta com esse nome$")
	public void souNotificadoQueJáExisteUmaContaComEsseNome() throws Throwable 
	{

		String titleContaExist = driver.findElement(By.xpath("//div[@class='alert alert-danger']")).getText();
		System.out.println("sr.barriga - titulo: " + titleContaExist);
		
		assertEquals("Já existe uma conta com esse nome!", titleContaExist);
	}
	
	@Então("^recebo a mensagem \"([^\"]*)\"$")
	public void receboAMensagem(String arg1) throws Throwable 
	{
	
		String titleContaExist = driver.findElement(By.xpath("//div[starts-with(@class, 'alert alert-')]")).getText();
		System.out.println("sr.barriga - titulo: " + titleContaExist);
		
		assertEquals(arg1, titleContaExist);
	}
	
	@Before(order = 0)
	public void AbrirBrowser0()
	{
		System.out.println("Opening ChromeDriver connection... FOR FUNCTION TEST");
	}
	
	@Before(order = 1)
	public void AbrirBrowser1()
	{
		System.out.println("Opening ChromeDriver connection... FOR FUNCTION TEST");
	}
	
		
	@After (order = 0, value = {"~@functionals"})
	public void fecharBrowser() 
	{
		System.out.println("closing ChromeDriver connection...... FOR FUNCTION TEST");
		driver.quit();
	}
	
	
	@After(order = 1, value = {"~@functionals"})
	public void screenshot(Scenario cenario) 
	{
		File screenshotAs = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshotAs, new File("target/screenshots/"+cenario.getId()+".jpg"));
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
}
